"""
pandas: data frame setup
seaborn(set up on matplotlib): graphing
RandomForestClassifier, SVC(support vector classifier), neural networks: common classifiers in sklearn
metrics: provide some metrics
StandardScaler: most commonly used preprocessing
train_test_split: split data into different sections
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import confusion_matrix, classification_report
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split

# loading dataset
"""
By using dataframe info() method, you can peak the basic information on the data content
such as null value existence, to deal with null values:
    1. delete them
    2. fill in with average
Pay attention to the percentage of null values to the whole data, 
it will decide whether you should just delete them or find another way to recollect data
"""
wine = pd.read_csv('data/winequality-red.csv', sep=';')
# print(wine.head())
# print(wine.info())
# print(wine.isnull().sum())


# preprocessing data (a simple way)
bins = (2, 6.5, 8)  # defines 2 bins: (2, 6.5], (6.5, 8]
group_names = ['bad', 'good']
wine['quality'] = pd.cut(wine['quality'], bins=bins, labels=group_names)  # cut based on bins and replace by labels
# print(wine['quality'].unique())

label_quality = LabelEncoder()
wine['quality'] = label_quality.fit_transform(wine['quality'])  # encode the labels into numbers, classification
# print(wine['quality'].value_counts())

# plotting
sns.countplot(wine['quality'])
plt.show()

# separate the dataset as response variables and feature variables
X = wine.drop('quality', axis=1)  # information used to predict the wine quality
y = wine['quality']               # wine quality itself, the ground truth

# train and test splitting of data (given a random seed 42)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# applying standard scaling to get optimized result
sc = StandardScaler()  # to scale the rest of the information, kind of a normalization
X_train = sc.fit_transform(X_train)
# we don't use fit_transform is because we already fit the data, and we want to use the same standard
X_test = sc.transform(X_test)


# Random Forest Classifier
rfc = RandomForestClassifier(n_estimators=200)
rfc.fit(X_train, y_train)
pred_rfc = rfc.predict(X_test)

# see how the model preformed
print("***classification report***")
print(classification_report(y_true=y_test, y_pred=pred_rfc))
print("***confusion matrix***")
print(confusion_matrix(y_true=y_test, y_pred=pred_rfc))


# SVM Classifier
clf = svm.SVC()
clf.fit(X_train, y_train)
pred_clf = clf.predict(X_test)

# see how the model preformed
print("***classification report***")
print(classification_report(y_true=y_test, y_pred=pred_clf))
print("***confusion matrix***")
print(confusion_matrix(y_true=y_test, y_pred=pred_clf))


# Neural Network
mlpc = MLPClassifier(hidden_layer_sizes=(11, 11, 11), max_iter=500)
mlpc.fit(X_train, y_train)
pred_mlpc = mlpc.predict(X_test)

# see how the model preformed
print("***classification report***")
print(classification_report(y_true=y_test, y_pred=pred_mlpc))
print("***confusion matrix***")
print(confusion_matrix(y_true=y_test, y_pred=pred_mlpc))


# accuracy score
print('*' * 25)
cm = accuracy_score(y_test, pred_rfc)
print('Random Forest Classifier Accuracy: ', cm)